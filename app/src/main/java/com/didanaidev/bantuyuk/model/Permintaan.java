package com.didanaidev.bantuyuk.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by milezoom on 22/11/15.
 */
public class Permintaan implements Parcelable {
    public static final Creator<Permintaan> CREATOR = new Creator<Permintaan>() {
        @Override
        public Permintaan createFromParcel(Parcel in) {
            return new Permintaan(in);
        }

        @Override
        public Permintaan[] newArray(int size) {
            return new Permintaan[size];
        }
    };
    private String judul;
    private String tipe;
    private String username;
    private int jarak;

    public Permintaan(String judul, String tipe, String username, int jarak) {
        this.judul = judul;
        this.tipe = tipe;
        this.username = username;
        this.jarak = jarak;
    }

    public Permintaan(Parcel in) {
        String[] data = new String[4];

        in.readStringArray(data);
        this.judul = data[0];
        this.tipe = data[1];
        this.username = data[2];
        this.jarak = Integer.parseInt(data[3]);
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getJarak() {
        return jarak;
    }

    public void setJarak(int jarak) {
        this.jarak = jarak;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(new String[]{this.judul, this.tipe, this.username, String.valueOf(this.jarak)});
    }
}
