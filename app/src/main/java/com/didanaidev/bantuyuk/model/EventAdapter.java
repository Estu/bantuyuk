package com.didanaidev.bantuyuk.model;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.didanaidev.bantuyuk.R;
import com.didanaidev.bantuyuk.controller.DetailEvent;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by milezoom on 30/11/15.
 */
public class EventAdapter extends RecyclerView.Adapter<EventAdapter.AktivitasViewHolder> {
    private ArrayList<Event> dataSet = null;

    public EventAdapter(ArrayList<Event> dataSet) {
        this.dataSet = dataSet;
    }

    @Override
    public AktivitasViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_event, parent, false);
        return new AktivitasViewHolder(v);
    }

    @Override
    public void onBindViewHolder(AktivitasViewHolder holder, final int position) {
        String judul, tanggal, lokasi_jarak = "";

        judul = String.valueOf(dataSet.get(position).getJudul());
        holder.judul.setText(judul);

        Locale locale = new Locale("in", "ID");
        DateFormat format = DateFormat.getDateInstance(DateFormat.LONG, locale);
        tanggal = format.format(dataSet.get(position).getTanggal());
        holder.tanggal.setText(tanggal);

        lokasi_jarak = dataSet.get(position).getLokasi() + " - " + dataSet.get(position).getJarak();
        holder.lokasi_jarak.setText(lokasi_jarak);

        String[] tipe = dataSet.get(position).getTipe();
        if (tipe[0].equals("tidak")) {
            holder.speak_up.setVisibility(View.GONE);
        }
        if (tipe[1].equals("tidak")) {
            holder.help_me.setVisibility(View.GONE);
        }
        if (tipe[2].equals("tidak")) {
            holder.funding.setVisibility(View.GONE);
        }
        holder.detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = position;
                Intent intent = new Intent(v.getContext(), DetailEvent.class);
                intent.putExtra("event", dataSet.get(pos));
                v.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public static class AktivitasViewHolder extends RecyclerView.ViewHolder {
        protected TextView judul;
        protected TextView tanggal;
        protected TextView lokasi_jarak;
        protected ImageView speak_up;
        protected ImageView help_me;
        protected ImageView funding;
        protected Button detail;

        public AktivitasViewHolder(View v) {
            super(v);
            judul = (TextView) v.findViewById(R.id.card_event_judul);
            tanggal = (TextView) v.findViewById(R.id.card_event_tanggal);
            lokasi_jarak = (TextView) v.findViewById(R.id.card_event_lokasi_jarak);
            speak_up = (ImageView) v.findViewById(R.id.card_event_tipe1);
            help_me = (ImageView) v.findViewById(R.id.card_event_tipe2);
            funding = (ImageView) v.findViewById(R.id.card_event_tipe3);
            detail = (Button) v.findViewById(R.id.card_event_btn_detail);
        }
    }
}
