package com.didanaidev.bantuyuk.model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.didanaidev.bantuyuk.R;

import java.util.ArrayList;

/**
 * Created by milezoom on 02/12/15.
 */
public class TipePermintaanAdapter extends ArrayAdapter<TipePermintaan> {
    private int groupId;
    private Context context;
    private ArrayList<TipePermintaan> dataSet;
    private LayoutInflater inflater;

    public TipePermintaanAdapter(Context context, int groupId, int id, ArrayList<TipePermintaan> dataSet) {
        super(context, id, dataSet);
        this.dataSet = dataSet;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.groupId = groupId;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View view = this.inflater.inflate(groupId, parent, false);

        ImageView iconTipe = (ImageView) view.findViewById(R.id.tipe_image);
        iconTipe.setImageResource(dataSet.get(position).getIconId());

        TextView name = (TextView) view.findViewById(R.id.tipe_name);
        name.setText(dataSet.get(position).getName());

        return view;
    }

    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getView(position, convertView, parent);
    }
}
