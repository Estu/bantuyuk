package com.didanaidev.bantuyuk.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by milezoom on 02/12/15.
 */
public class TipePermintaan implements Parcelable {
    public static final Creator<TipePermintaan> CREATOR = new Creator<TipePermintaan>() {
        @Override
        public TipePermintaan createFromParcel(Parcel source) {
            return new TipePermintaan(source);
        }

        @Override
        public TipePermintaan[] newArray(int size) {
            return new TipePermintaan[size];
        }
    };
    private String name;
    private int iconId;

    public TipePermintaan(String name, int iconId) {
        this.name = name;
        this.iconId = iconId;
    }

    public TipePermintaan(Parcel in) {
        this.name = in.readString();
        this.iconId = in.readInt();
    }

    public int getIconId() {
        return iconId;
    }

    public void setIconId(int iconId) {
        this.iconId = iconId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeInt(this.iconId);
    }
}
