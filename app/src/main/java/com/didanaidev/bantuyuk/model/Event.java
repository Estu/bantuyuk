package com.didanaidev.bantuyuk.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * Created by milezoom on 30/11/15.
 */
public class Event implements Parcelable {

    public static final Creator<Event> CREATOR = new Creator<Event>() {
        @Override
        public Event createFromParcel(Parcel source) {
            return new Event(source);
        }

        @Override
        public Event[] newArray(int size) {
            return new Event[size];
        }
    };
    private String judul;
    private String username;
    private Date tanggal;
    private String lokasi;
    private String jarak;
    private String[] tipe;

    public Event(String judul, String username, Date tanggal, String lokasi, String jarak, String[] tipe) {
        this.judul = judul;
        this.username = username;
        this.tanggal = tanggal;
        this.lokasi = lokasi;
        this.jarak = jarak;
        this.tipe = tipe;
    }

    public Event(Parcel in) {
        this.judul = in.readString();
        this.username = in.readString();
        this.tanggal = new Date(in.readLong());
        this.lokasi = in.readString();
        this.jarak = in.readString();
        this.tipe = in.createStringArray();
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getTanggal() {
        return tanggal;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }

    public String getLokasi() {
        return lokasi;
    }

    public void setLokasi(String lokasi) {
        this.lokasi = lokasi;
    }

    public String getJarak() {
        return jarak;
    }

    public void setJarak(String jarak) {
        this.jarak = jarak;
    }

    public String[] getTipe() {
        return tipe;
    }

    public void setTipe(String[] tipe) {
        this.tipe = tipe;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.judul);
        dest.writeString(this.username);
        dest.writeLong(this.tanggal.getTime());
        dest.writeString(this.lokasi);
        dest.writeString(this.jarak);
        dest.writeStringArray(this.tipe);
    }
}
