package com.didanaidev.bantuyuk.model;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.didanaidev.bantuyuk.R;
import com.didanaidev.bantuyuk.controller.DetailPermintaan;

import java.util.ArrayList;

/**
 * Created by milezoom on 09/11/15.
 */
public class PermintaanAdapter extends RecyclerView.Adapter<PermintaanAdapter.PermintaanViewHolder> {
    private ArrayList<Permintaan> dataSet = null;

    public PermintaanAdapter(ArrayList<Permintaan> dataSet) {
        this.dataSet = dataSet;
    }

    @Override
    public PermintaanViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_permintaan, parent, false);
        return new PermintaanViewHolder(v);
    }

    @Override
    public void onBindViewHolder(PermintaanViewHolder holder, final int position) {
        String jarak, judul, username = "";

        jarak = String.valueOf(dataSet.get(position).getJarak())
                + holder.itemView.getContext().getString(R.string.tambahan_permintaan_jarak);
        holder.jarak.setText(jarak);
        judul = holder.itemView.getContext().getString(R.string.tambahan_permintaan_judul)
                + " " + dataSet.get(position).getJudul();
        holder.judul.setText(judul);
        switch (dataSet.get(position).getTipe()) {
            case "speak-up":
                holder.tipe.setImageResource(R.drawable.speak_up_icon);
                break;
            case "help-me":
                holder.tipe.setImageResource(R.drawable.help_me_icon);
                break;
        }
        holder.foto.setImageResource(R.drawable.perokok);
        switch (dataSet.get(position).getUsername()) {
            case "aulia":
                username = holder.itemView.getContext().getString(R.string.tambahan_permintaan_user) + " Aulia";
                holder.user.setText(username);
                break;
            case "estu":
                username = holder.itemView.getContext().getString(R.string.tambahan_permintaan_user) + " Estu";
                holder.user.setText(username);
                break;
            case "riswan":
                username = holder.itemView.getContext().getString(R.string.tambahan_permintaan_user) + " Riswan";
                holder.user.setText(username);
                break;
            case "summayah":
                username = holder.itemView.getContext().getString(R.string.tambahan_permintaan_user) + " Summayah";
                holder.user.setText(username);
                break;
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setSelected(true);
                int pos = position;
                Intent intent = new Intent(v.getContext(), DetailPermintaan.class);
                intent.putExtra("permintaan", dataSet.get(pos));
                v.getContext().startActivity(intent);
                ((Activity) v.getContext()).overridePendingTransition(R.anim.slide_in_right, R.anim.fade_back);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textView;

        public ViewHolder(TextView v) {
            super(v);
            textView = v;
        }
    }

    public static class PermintaanViewHolder extends RecyclerView.ViewHolder {
        protected TextView judul;
        protected TextView jarak;
        protected TextView user;
        protected ImageView tipe;
        protected ImageView foto;

        public PermintaanViewHolder(View v) {
            super(v);
            judul = (TextView) v.findViewById(R.id.permintaan_judul);
            jarak = (TextView) v.findViewById(R.id.permintaan_jarak);
            tipe = (ImageView) v.findViewById(R.id.permintaan_tipe);
            user = (TextView) v.findViewById(R.id.permintaan_user);
            foto = (ImageView) v.findViewById(R.id.permintaan_foto);
        }
    }
}