package com.didanaidev.bantuyuk.controller;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.didanaidev.bantuyuk.R;
import com.didanaidev.bantuyuk.helper.DividerItemDecoration;
import com.didanaidev.bantuyuk.model.Permintaan;
import com.didanaidev.bantuyuk.model.PermintaanAdapter;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by milezoom on 01/11/15.
 */
public class DaftarPermintaanFragment extends Fragment {

    private ArrayList<Permintaan> permintaanDataSet = new ArrayList<Permintaan>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Permintaan p1 = new Permintaan("Usir Perokok", "speak-up", "aulia", 500);
        Permintaan p2 = new Permintaan("Main Drum", "help-me", "estu", 600);
        Permintaan p3 = new Permintaan("Pinjam Kalkulator", "help-me", "riswan", 623);
        Permintaan p4 = new Permintaan("Usir Pembakar Sampah", "speak-up", "summayah", 820);
        Permintaan p5 = new Permintaan("Beri Makanan", "help-me", "estu", 900);
        Permintaan p6 = new Permintaan("Main Basket", "help-me", "riswan", 920);
        Permintaan p7 = new Permintaan("Semangatin", "speak-up", "aulia", 950);

        permintaanDataSet.addAll(Arrays.asList(p1, p2, p3, p4, p5, p6, p7));

        View rootView = inflater.inflate(R.layout.fragment_daftar_permintaan, container, false);
        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.fragment_daftar_permintaan);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(new PermintaanAdapter(permintaanDataSet));
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), null));
        recyclerView.setHasFixedSize(true);
        return rootView;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isResumed()) {
            onResume();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!getUserVisibleHint()) {
            return;
        }

        HomeActivity homeActivity = (HomeActivity) getActivity();
        homeActivity.getFab().setVisibility(View.GONE);
    }
}
