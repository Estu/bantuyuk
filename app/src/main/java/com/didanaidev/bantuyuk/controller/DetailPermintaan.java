package com.didanaidev.bantuyuk.controller;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.didanaidev.bantuyuk.R;
import com.didanaidev.bantuyuk.model.Permintaan;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class DetailPermintaan extends AppCompatActivity implements OnMapReadyCallback {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_permintaan);

        Permintaan permintaan = getIntent().getExtras().getParcelable("permintaan");

        TextView judul = (TextView) findViewById(R.id.detail_permintaan_judul);
        TextView username = (TextView) findViewById(R.id.detail_permintaan_username);
        ImageView user_image = (ImageView) findViewById(R.id.detail_permintaan_user_image);

        judul.setText(permintaan.getJudul());
        String user = "";
        switch (permintaan.getUsername()) {
            case "aulia":
                user = this.getString(R.string.tambahan_permintaan_user) + " Aulia";
                username.setText(user);
                user_image.setImageResource(R.drawable.aulia_cropped);

                break;
            case "estu":
                user = this.getString(R.string.tambahan_permintaan_user) + " Estu";
                username.setText(user);
                user_image.setImageResource(R.drawable.estu_cropped);
                break;
            case "riswan":
                user = this.getString(R.string.tambahan_permintaan_user) + " Riswan";
                username.setText(user);
                user_image.setImageResource(R.drawable.riswan_cropped);
                break;
            case "summayah":
                user = this.getString(R.string.tambahan_permintaan_user) + " Summayah";
                username.setText(user);
                user_image.setImageResource(R.drawable.summayah_cropped);
                break;
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        ImageButton btnBack = (ImageButton) findViewById(R.id.detail_permintaan_action_back);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getSupportActionBar().hide();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                overridePendingTransition(R.anim.fade_forward, R.anim.slide_out_right);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.fade_forward, R.anim.slide_out_right);
    }

    @Override
    public void onMapReady(GoogleMap map) {

        LatLng myLoc = new LatLng(-6.3615231, 106.8230758);
        LatLng theyLoc = new LatLng(-6.3615231, 106.8230758);

        map.addMarker(new MarkerOptions()
                .position(myLoc)
                .title("Lokasi Kamu"));
        map.addMarker(new MarkerOptions()
                .position(theyLoc)
                .title("Lokasi Dia")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
        map.addCircle(new CircleOptions()
                .center(myLoc)
                .radius(1000)
                .strokeColor(Color.RED)
                .strokeWidth(2)
                .fillColor(Color.argb(75, 0, 0, 0)));


        map.moveCamera(CameraUpdateFactory.newLatLng(myLoc));
        map.animateCamera(CameraUpdateFactory.zoomTo(14), 2000, null);

    }
}

