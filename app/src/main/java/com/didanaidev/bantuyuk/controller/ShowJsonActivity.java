package com.didanaidev.bantuyuk.controller;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.didanaidev.bantuyuk.R;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public class ShowJsonActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_json);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        final GetJSON gj = new GetJSON();

        Button btnTest = (Button) findViewById(R.id.btn_test);

        btnTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView resultJson = (TextView) findViewById(R.id.result_json);
                EditText ipAddress = (EditText) findViewById(R.id.input_ip_address);
                resultJson.setText(gj.doInBackground());
            }
        });
    }

    private String getJSON(String ip) {
        String resultString = "(kosong)";

        try {
            URL url = new URL("http://" + ip + "/event/get/1");
            URLConnection connect = url.openConnection();

            HttpURLConnection httpConnect = (HttpURLConnection) connect;
            httpConnect.setAllowUserInteraction(false);
            httpConnect.setInstanceFollowRedirects(true);
            httpConnect.setRequestMethod("GET");
            httpConnect.connect();

            InputStream stream = httpConnect.getInputStream();
            resultString = stream.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultString;
    }

    private class GetJSON extends AsyncTask<String, String, String> {
        HttpURLConnection urlConnection;

        @Override
        protected String doInBackground(String... params) {
            StringBuilder result = new StringBuilder();

            try {
                URL url = new URL("http://10.0.2.2:8011/event/get/all/1");
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                BufferedReader reader = new BufferedReader(new InputStreamReader(in));

                String line;
                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                urlConnection.disconnect();
            }


            return result.toString();
        }

        @Override
        protected void onPostExecute(String result) {

        }
    }
}
