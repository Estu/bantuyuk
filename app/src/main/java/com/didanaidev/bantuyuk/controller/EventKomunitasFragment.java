package com.didanaidev.bantuyuk.controller;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.didanaidev.bantuyuk.R;
import com.didanaidev.bantuyuk.model.Event;
import com.didanaidev.bantuyuk.model.EventAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

/**
 * Created by milezoom on 01/11/15.
 */
public class EventKomunitasFragment extends Fragment {
    private ArrayList<Event> eventDataSet = new ArrayList<Event>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Event a1
                = new Event("Pembersihan Sungai Ciliwung", "Komunitas Kali Bersih", new Date(), "Kali Ciliwung", "1,2km", new String[]{"tidak", "ya", "ya"});
        Event a2
                = new Event("Demo Anti Korupsi", "Komunitas Anti Korupsi", new Date(), "Bunderan HI", "12,5km", new String[]{"ya", "tidak", "tidak"});

        eventDataSet.addAll(Arrays.asList(a1, a2));

        View rootView = inflater.inflate(R.layout.fragment_event_komunitas, container, false);
        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.fragment_daftar_aktivitas);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(new EventAdapter(eventDataSet));
        recyclerView.setHasFixedSize(true);
        return rootView;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isResumed()) {
            onResume();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!getUserVisibleHint()) {
            return;
        }

        HomeActivity homeActivity = (HomeActivity) getActivity();
        homeActivity.getFab().setVisibility(View.VISIBLE);
        homeActivity.getFab().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), BuatEvent.class);
                v.getContext().startActivity(intent);
            }
        });
    }
}
