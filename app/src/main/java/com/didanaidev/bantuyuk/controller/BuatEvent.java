package com.didanaidev.bantuyuk.controller;

import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;

import com.didanaidev.bantuyuk.R;
import com.didanaidev.bantuyuk.model.TipePermintaan;
import com.didanaidev.bantuyuk.model.TipePermintaanAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class BuatEvent extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buat_event);

        ArrayList<TipePermintaan> dataSet = new ArrayList<>();
        dataSet.add(new TipePermintaan("Speak Up", R.drawable.speak_up_icon));
        dataSet.add(new TipePermintaan("Help Me", R.drawable.help_me_icon));
        dataSet.add(new TipePermintaan("Funding", R.drawable.funding_icon));

        TipePermintaanAdapter adapter = new TipePermintaanAdapter(this, R.layout.tipe_permintaan_spinner_item, R.id.tipe_name, dataSet);

        Spinner tipeSpinner = (Spinner) findViewById(R.id.buat_event_tipe);
        tipeSpinner.setAdapter(adapter);
        tipeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Date today = Calendar.getInstance().getTime();
        Locale locale = new Locale("in", "ID");
        SimpleDateFormat formatTanggal = new SimpleDateFormat("EEE, MMM dd yyyy", locale);
        SimpleDateFormat formatJam = new SimpleDateFormat("hh:mm", locale);

        TextView eventTanggal = (TextView) findViewById(R.id.buat_event_tanggal);
        TextView eventJam = (TextView) findViewById(R.id.buat_event_jam);

        eventTanggal.setText(formatTanggal.format(today));
        eventJam.setText(formatJam.format(today));

        getSupportActionBar().setTitle("Buat Event Baru");
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
