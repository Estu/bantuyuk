package com.didanaidev.bantuyuk.controller;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.didanaidev.bantuyuk.R;
import com.didanaidev.bantuyuk.model.TipePermintaan;
import com.didanaidev.bantuyuk.model.TipePermintaanAdapter;


import java.io.File;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

public class BuatPermintaan extends AppCompatActivity {
    public final static int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1034;
    public final String APP_TAG = "MyCustomApp";
    public String photoFileName = "photo.jpg";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buat_permintaan);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        ArrayList<TipePermintaan> dataSet = new ArrayList<>();
        dataSet.add(new TipePermintaan("Speak Up", R.drawable.speak_up_icon));
        dataSet.add(new TipePermintaan("Help Me", R.drawable.help_me_icon));
        dataSet.add(new TipePermintaan("Funding", R.drawable.funding_icon));

        TipePermintaanAdapter adapter = new TipePermintaanAdapter(this, R.layout.tipe_permintaan_spinner_item, R.id.tipe_name, dataSet);

        Spinner tipeSpinner = (Spinner) findViewById(R.id.buat_permintaan_tipe);
        tipeSpinner.setAdapter(adapter);
        tipeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //String tipePermintaan = dataSet.get(position).getName();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        getSupportActionBar().setTitle("Buat Permintaan Baru");
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        EditText ed = (EditText) findViewById(R.id.capture_gambar);
        ed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onLaunchCamera(v);
        Button btnBuat = (Button) findViewById(R.id.buat_permintaan_btn_buat);
        final SendPostReqAsyncTask posting = new SendPostReqAsyncTask();
        btnBuat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText inputPermintaanJudul = (EditText) findViewById(R.id.input_permintaan_judul);
                EditText inputPermintaanDeskripsi = (EditText) findViewById(R.id.input_permintaan_deskripsi);
                //EditText inputPermintaanTipe = (EditText) findViewById(R.id.buat_permintaan_tipe);
                TextView message_test = (TextView) findViewById(R.id.message_test);
                message_test.setText(posting.doInBackground());
            }
        });
    }

    public void onLaunchCamera(View view) {
        // create Intent to take a picture and return control to the calling application
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, getPhotoFileUri(photoFileName)); // set the image file name

        // If you call startActivityForResult() using an intent that no app can handle, your app will crash.
        // So as long as the result is not null, it's safe to use the intent.
        if (intent.resolveActivity(getPackageManager()) != null) {
            // Start the image capture intent to take photo
            startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Uri takenPhotoUri = getPhotoFileUri(photoFileName);
                // by this point we have the camera photo on disk
                Bitmap takenImage = BitmapFactory.decodeFile(takenPhotoUri.getPath());
                // Load the taken image into a preview
                ImageView ivPreview = (ImageView) findViewById(R.id.buat_permintaan_gambar_peristiwa);
                ivPreview.setImageBitmap(takenImage);
            } else { // Result was a failure
                Toast.makeText(this, "Picture wasn't taken!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    // Returns the Uri for a photo stored on disk given the fileName
    public Uri getPhotoFileUri(String fileName) {
        // Only continue if the SD Card is mounted
        if (isExternalStorageAvailable()) {
            // Get safe storage directory for photos
            File mediaStorageDir = new File(
                    Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), APP_TAG);

            // Create the storage directory if it does not exist
            if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()) {
                Log.d(APP_TAG, "failed to create directory");
            }

            // Return the file target for the photo based on filename
            return Uri.fromFile(new File(mediaStorageDir.getPath() + File.separator + fileName));
        }
        return null;
    }

    private boolean isExternalStorageAvailable() {
        String state = Environment.getExternalStorageState();
        if (state.equals(Environment.MEDIA_MOUNTED)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.fade_forward, R.anim.slide_out_right);
    }

    class SendPostReqAsyncTask extends AsyncTask<String, Void, String> {
        StringBuilder sb = new StringBuilder();
        String pesan = "kosong";
        @Override
        protected String doInBackground(String... params) {
            URL url;
            HttpURLConnection urlConn;
            DataOutputStream printout;
            DataInputStream input;
            BufferedOutputStream os = null;
            try {
                //Create JSONObject here
                JSONObject jsonParam = new JSONObject();
                jsonParam.put("ID", "25");
                jsonParam.put("description", "Real");
                jsonParam.put("enable", "true");

                String content = jsonParam.toString();

                url = new URL("http://10.0.2.2:8011/event/post");
                urlConn = (HttpURLConnection) url.openConnection();
                urlConn.setDoInput(true);
                urlConn.setDoOutput(true);
                urlConn.setUseCaches(false);
                urlConn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                urlConn.setRequestProperty("Connection", "close");
                //urlConn.setRequestProperty("Host", "android.schoolportal.gr");
                urlConn.setRequestMethod("POST");

                urlConn.connect();

                os = new BufferedOutputStream(urlConn.getOutputStream());
                os.write(content.getBytes());

                os.flush();

                pesan = "success";
                int HttpResult =urlConn.getResponseCode();
                pesan = HttpResult+"";
                if(HttpResult ==HttpURLConnection.HTTP_OK){
                    BufferedReader br = new BufferedReader(new InputStreamReader(
                            urlConn.getInputStream(),"utf-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    br.close();

                    System.out.println("" + sb.toString());
                    //pesan = "OK";
                }else{
                    System.out.println(urlConn.getResponseMessage());
                    //pesan = "Ga OK";
                }
            } catch ( Exception e) {
                e.printStackTrace();
            } finally {
                try{
                    os.close();
                } catch ( IOException e){
                    e.printStackTrace();
                }
            }
            return pesan;
        }
    }
}
